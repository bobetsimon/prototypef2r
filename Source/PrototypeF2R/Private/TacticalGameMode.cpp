// Fill out your copyright notice in the Description page of Project Settings.

#include "TacticalGameMode.h"
#include "Core/Team.h"
#include "Cursor.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"

void ATacticalGameMode::BeginPlay()
{
	Super::BeginPlay();

	m_Cursor = Cast<ACursor>(UGameplayStatics::GetPlayerPawn(this, 0));
}

void ATacticalGameMode::Tick(float _DeltaTime)
{
	Super::Tick(_DeltaTime);

	if (m_CurrentPlayingTeam == nullptr && m_Teams.Num() > 0)
	{
		m_CurrentPlayingTeam = m_Teams[m_CurrentTeamIndex];
		if (m_CurrentPlayingTeam != nullptr)
		{
			m_CurrentPlayingTeam->StartTeamTurn();
		}
	}

	if (m_ShouldReorder)
	{
		ReorderTeams();
	}
}

void ATacticalGameMode::ReorderTeams()
{
	//Sorting teams by ID
	m_Teams.Sort([](const UTeam& LHS, const UTeam& RHS) { return LHS.GetTeamID() > RHS.GetTeamID(); });
	m_ShouldReorder = false;
}

void ATacticalGameMode::UpdatePlayingTeam()
{
	m_CurrentTeamIndex++;
	if (m_CurrentTeamIndex >= m_Teams.Num())
	{
		m_CurrentTeamIndex = 0;
	}
	if (m_Teams.Num() > 0 && m_CurrentTeamIndex < m_Teams.Num())
	{
		m_CurrentPlayingTeam = m_Teams[m_CurrentTeamIndex];
		m_CurrentPlayingTeam->StartTeamTurn();
		m_Cursor->ShowCharacterSelectionFeedback(true);
	}
}