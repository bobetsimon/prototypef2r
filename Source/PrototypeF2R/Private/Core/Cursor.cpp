// Fill out your copyright notice in the Description page of Project Settings.

#include "Cursor.h"
#include "Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include "TileComponent.h"
#include "Runtime/Engine/Classes/Components/InputComponent.h"
#include "TacticalGameMode.h"
#include "Team.h"
#include "Runtime/Engine/Classes/Engine/Engine.h"
#include "ProtoCharacter.h"


// Sets default values
ACursor::ACursor()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ACursor::BeginPlay()
{
	Super::BeginPlay();

	m_GameMode = Cast<ATacticalGameMode>(GetWorld()->GetAuthGameMode());
}

// Called every frame
void ACursor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	RefreshCurrentTile();
	if (!IsOnMovement())
	{
		SnapOnCurrentTile();
	}

	FirstTeamInitialization();
}

void ACursor::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up game play key bindings
	check(PlayerInputComponent);

	PlayerInputComponent->BindAction("SwitchCharacterLeft", EInputEvent::IE_Pressed, this, &ACursor::SwitchToPreviousCharacter);
	PlayerInputComponent->BindAction("SwitchCharacterRight", EInputEvent::IE_Pressed, this, &ACursor::SwitchToNextCharacter);
	PlayerInputComponent->BindAction("ConfirmAction", EInputEvent::IE_Pressed, this, &ACursor::ConfirmAction);
	PlayerInputComponent->BindAction("CancelAction", EInputEvent::IE_Pressed, this, &ACursor::CancelAction);
	PlayerInputComponent->BindAction("EndTurn", EInputEvent::IE_Pressed, this, &ACursor::EndTurn);
	PlayerInputComponent->BindAction("SwitchActionRight", EInputEvent::IE_Pressed, this, &ACursor::SwitchToNextAction);
	PlayerInputComponent->BindAction("SwitchActionLeft", EInputEvent::IE_Pressed, this, &ACursor::SwitchToPreviousAction);

	PlayerInputComponent->BindAxis("Right", this, &ACursor::MoveRight);
	PlayerInputComponent->BindAxis("Up", this, &ACursor::MoveUp);

}

void ACursor::MoveRight(float _Value)
{
	APlayerController* playerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	FVector right = playerController->PlayerCameraManager->GetActorRightVector();
	right.Z = 0;

	FVector moveDirection = right;
	moveDirection *= _Value;
	moveDirection *= m_CursorSpeed;

	SetActorLocation(GetActorLocation() + moveDirection);

}

void ACursor::MoveUp(float _Value)
{
	APlayerController* playerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	FVector forward = playerController->PlayerCameraManager->GetActorForwardVector();
	forward.Z = 0;

	FVector moveDirection = forward;
	moveDirection *= _Value;
	moveDirection *= m_CursorSpeed;

	SetActorLocation(GetActorLocation() + moveDirection);

}

//Refreshes current, changing the new and old ones
void ACursor::RefreshCurrentTile()
{
	FHitResult hit;
	FCollisionQueryParams params;
	FVector start = GetActorLocation();
	FVector end = GetActorLocation();
	end.Z = -2000000.0f;

	params.AddIgnoredActor(this);


	if (GetWorld()->LineTraceSingleByChannel(hit, start, end, ECollisionChannel::ECC_WorldDynamic, params))
	{
		UTileComponent *hitTile = Cast<UTileComponent>(hit.GetComponent());

		if (hitTile != nullptr)
		{
			m_CurrentTile = hitTile;
			if (m_OldTile != nullptr)
			{
				if (m_OldTile != m_CurrentTile)
				{
					m_CurrentTile->SetSelected(true);
					m_OldTile->SetSelected(false);
					m_OldTile = m_CurrentTile;
				}
			}
			else
			{
				m_OldTile = m_CurrentTile;
			}
		}
	}
}

//Checks if our cursor is currently moving by the player input
bool ACursor::IsOnMovement()
{
	if (InputComponent != nullptr)
	{
		FVector2D movement = FVector2D(InputComponent->GetAxisValue(TEXT("Right")), InputComponent->GetAxisValue(TEXT("Up")));

		return (movement != FVector2D::ZeroVector);
	}
	return (false);
}

//Snaps the cursor on the current tile (for better controls of what we're doing)
void ACursor::SnapOnCurrentTile()
{
	if (m_CurrentTile != nullptr)
	{
		FVector current = GetActorLocation();
		FVector target = m_CurrentTile->GetComponentLocation();
		target.Z = current.Z;

		FVector newLocation = FMath::VInterpConstantTo(current, target, GetWorld()->GetDeltaSeconds(), m_SnappingSpeed);
		SetActorLocation(newLocation);
	}
}

//Switch to next character
void ACursor::SwitchToNextCharacter()
{
	SwitchCharacter(true);
}

//Switch to previous character
void ACursor::SwitchToPreviousCharacter()
{
	SwitchCharacter(false);
}

//Used to switch to the next or previous character depending on the boolean passed as parameter
void ACursor::SwitchCharacter(bool _IsNext)
{
	if (!IsTurnEnding())
	{
		if (m_CurrentConfirmedAction == nullptr)
		{
			if (!m_GameMode->AreActionsLimitedForEachCharacter())
			{
				if (m_CurrentTeam != nullptr)
				{
					if (m_CurrentTeam->GetTeammates().Num() > 0)
					{
						//Setting the index to add depending on boolean passed as parameter
						int nextIndexAdd = _IsNext ? 1 : -1;

						m_CurrentCharacterIndex += nextIndexAdd;
						if (m_CurrentCharacterIndex >= m_CurrentTeam->GetTeammates().Num())
						{
							m_CurrentCharacterIndex = 0;
						}
						if (m_CurrentCharacterIndex < 0)
						{
							m_CurrentCharacterIndex = m_CurrentTeam->GetTeammates().Num() - 1;
						}
						m_CurrentCharacter = m_CurrentTeam->GetTeammates()[m_CurrentCharacterIndex];
						ShowCharacterSelectionFeedback();
					}
					else
					{
						GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Orange, "Make sure your team has at least one member...");
					}
				}
				else
				{
					GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, "This shouldn't happen, call a programmer :3 !");
				}
			}
			else
			{
				GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Orange, "Make sure you're in the right game mode, cause you cannot switch right now.");
			}
		}
	}
}

//This is called most likely only once, or at least until the team is set, also prints what's going wrong whenever it needs to
void ACursor::FirstTeamInitialization()
{
	if (m_CurrentTeam == nullptr && !m_GameMode->ShouldReorderTeam())
	{
		TArray<UTeam*> &team = m_GameMode->GetTeams();
		if (team.Num() > 0)
		{
			for (int32 i = 0; i < team.Num(); i++)
			{
				if (team[i]->GetTeammates().Num() > 0)
				{
					m_CurrentTeam = team[i];
					m_CurrentCharacter = m_CurrentTeam->GetTeammates()[0];
					ShowCharacterSelectionFeedback(true);
					ResetAction();
				}
				else
				{
					GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red,
						"Make sure the team number " +
						FString::FromInt(i) +
						" has at least one member. Skipping straight to next team.");
				}
			}

		}
		else
		{
			if (GEngine != nullptr)
			{
				GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, "You must have at least one team on the map to use the cursor...");
			}
		}
	}
}

bool ACursor::IsTurnEnding()
{
	return (m_CurrentTeam != nullptr && m_CurrentTeam->IsEndingTurn());
}

//Confirms action
void ACursor::ConfirmAction()
{
	if (!IsTurnEnding())
	{
		if (m_CurrentSelectedAction != nullptr)
		{
			if (m_CurrentConfirmedAction != nullptr)
			{
				if (m_CurrentTeam != nullptr && m_CurrentCharacter != nullptr)
				{
					if (!m_GameMode->AreActionsPlayingInstantly())
					{
						//Timeline management
						AddActionInTimeline(m_CurrentConfirmedAction, m_CurrentCharacter);

					}
					m_CurrentConfirmedAction->SetTargetTile(m_CurrentCharacter, m_CurrentTile);
					m_CurrentTeam->RegisterUnitAction(m_CurrentCharacter, m_CurrentConfirmedAction);
					//just to refresh action count etc.
					ShowCharacterSelectionFeedback();
				}
				//TODO: cancel the tiles limit access of the previous confirmed action before setting it to null...
				m_CurrentConfirmedAction = nullptr;
			}
			else
			{
				m_CurrentConfirmedAction = m_CurrentSelectedAction;
				//TODO: limit tiles accesses depending on the current selected action
			}
		}
	}
}

//Cancels last action
void ACursor::CancelAction()
{
	if (!IsTurnEnding())
	{
		if (m_CurrentConfirmedAction != nullptr)
		{
			//TODO: cancel the tiles limit access of the previous confirmed action before setting it to null...
			m_CurrentConfirmedAction = nullptr;
		}
		else
		{
			if (m_CurrentTeam != nullptr && m_CurrentCharacter != nullptr)
			{
				if (!m_GameMode->AreActionsPlayingInstantly())
				{
					m_CurrentTeam->UnregisterUnitLastAction(m_CurrentCharacter);
					//to refresh action count lol.
					ShowCharacterSelectionFeedback();
					RemoveLastActionFromTimeline(m_CurrentCharacter);
				}
			}
		}
	}
}

//Ends the current turn
void ACursor::EndTurn()
{
	if (!IsTurnEnding())
	{
		m_CurrentTeam->EndTurn();
	}
}

//Switches to the next action available in the character's list
void ACursor::SwitchToPreviousAction()
{
	SwitchAction(false);
}

//Switches to the previous action available in the character's list
void ACursor::SwitchToNextAction()
{
	SwitchAction(true);
}

void ACursor::ResetAction()
{
	m_CurrentSelectedAction = m_CurrentCharacter->GetPossibleActions()[0];
	ShowActionSelectionFeedback();
}

void ACursor::SwitchAction(bool _IsNext)
{
	if (!IsTurnEnding())
	{
		if (m_CurrentConfirmedAction == nullptr)
		{
			int32 switchAction = _IsNext ? -1 : 1;
			if (m_CurrentCharacter != nullptr)
			{
				if (m_CurrentCharacter->GetPossibleActions().Num() > 0)
				{
					m_CurrentSelectionActionIndex += switchAction;
					if (m_CurrentSelectionActionIndex >= m_CurrentCharacter->GetPossibleActions().Num())
					{
						m_CurrentSelectionActionIndex = 0;
					}
					if (m_CurrentSelectionActionIndex < 0)
					{
						m_CurrentSelectionActionIndex = m_CurrentCharacter->GetPossibleActions().Num() - 1;
					}
					m_CurrentSelectedAction = m_CurrentCharacter->GetPossibleActions()[m_CurrentSelectionActionIndex];
					ShowActionSelectionFeedback();
				}
			}
		}
	}
}