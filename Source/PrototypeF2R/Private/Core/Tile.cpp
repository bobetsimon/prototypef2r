// Fill out your copyright notice in the Description page of Project Settings.

#include "Public/Core/Tile.h"
#include "Runtime/Engine/Classes/Engine/StaticMesh.h"
#include "Runtime/Engine/Classes/Components/StaticMeshComponent.h"
#include "Runtime/Engine/Classes/Materials/MaterialInstance.h"

// Sets default values
ATile::ATile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	m_StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>("TileMesh");	
}

// Called when the game starts or when spawned
void ATile::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATile::SetSelected(bool _IsSelected)
{
	if (m_StaticMeshComponent->GetStaticMesh() != nullptr)
	{
		if (_IsSelected)
		{
			m_StaticMeshComponent->SetMaterial(0, m_SelectedColor);
		}
		else
		{
			m_StaticMeshComponent->SetMaterial(0, m_BaseColor);
		}
	}
}