// Fill out your copyright notice in the Description page of Project Settings.

#include "TimelineAction.h"
#include "ProtoCharacter.h"
#include "TileComponent.h"

void UTimelineAction::SetTargetTile(AProtoCharacter *_CharacterToExecuteAction, UTileComponent *_Tile)
{
	if (_Tile != nullptr && _CharacterToExecuteAction != nullptr)
	{
		m_TargetTile = _Tile;
	}
}

void UTimelineAction::Execute(AProtoCharacter *_CharacterToExecuteAction)
{
	_CharacterToExecuteAction->ChangeMoveTarget(m_TargetTile->GetComponentLocation());
	_CharacterToExecuteAction->ChangeCurrentAction(m_ActionEnumValue);
}