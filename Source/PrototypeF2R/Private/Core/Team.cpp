// Fill out your copyright notice in the Description page of Project Settings.

#include "Team.h"
#include "ProtoCharacter.h"
#include "Engine/Classes/Engine/World.h"
#include "TacticalGameMode.h"
#include "Cursor.h"
#include "Engine/Engine.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"

// Sets default values for this component's properties
UTeam::UTeam()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	// ...
}


// Called when the game starts
void UTeam::BeginPlay()
{
	Super::BeginPlay();

	m_TeamMembersActionsBuffer = TArray<FCharacterActions>();

	//Getting the game mode and adding each team into it, so it can manage the turns properly
	m_GameMode = Cast<ATacticalGameMode>(GetWorld()->GetAuthGameMode());
	if (m_GameMode != nullptr)
	{
		m_GameMode->GetTeams().Add(this);
		m_GameMode->SetShouldReorderTeam(true);
	}

	//We add each teammate in the array which has the character and their actions linked
	for (int i = 0; i < m_Teammate.Num(); i++)
	{
		AddUnit(m_Teammate[i]);
	}
}


// Called every frame
void UTeam::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	//if the game mode is made to not play actions instantly then we manage it when the end turn flag is set
	if (m_IsEndingTurn && !m_GameMode->AreActionsPlayingInstantly())
	{
		ManageTurnEnding();
	}
}

//we'll keep playing all the actions of the time line while there are some to play
//maybe there's a cleaner way to do it, but it's Sunday night and I want it to be done for good.
void UTeam::ManageTurnEnding()
{
	bool shouldSkipToNextTimelineActions = true;
	bool hasAnyActionLeft = false;

	//checking if anyone is currently doing an action that isn't waiting...
	for (int32 i = 0; i < m_TeamMembersActionsBuffer.Num() && shouldSkipToNextTimelineActions; i++)
	{
		AProtoCharacter *currentCharacter = m_TeamMembersActionsBuffer[i].m_ProtoCharacter;
		EActionEnum action = currentCharacter->GetCurrentActionState();
		if (action != EActionEnum::AE_None)
		{
			shouldSkipToNextTimelineActions = false;
		}
	}

	//if all the actions of the current time line "turn" are done, then we skip to the next one
	//all the characters who have actions left are doing them...
	if (shouldSkipToNextTimelineActions)
	{
		for (int32 i = 0; i < m_TeamMembersActionsBuffer.Num(); i++)
		{
			AProtoCharacter *currentCharacter = m_TeamMembersActionsBuffer[i].m_ProtoCharacter;
			if (m_TeamMembersActionsBuffer[i].m_Actions.Num() > 0)
			{
				m_TeamMembersActionsBuffer[i].m_Actions.RemoveAt(0);
				if (m_TeamMembersActionsBuffer[i].m_Actions.Num() > 0)
				{
					currentCharacter->DoAction(m_TeamMembersActionsBuffer[i].m_Actions[0]);
					hasAnyActionLeft = true;
				}
			}
		}
	}

	//when it's all done then we skip to the next team...
	if (!hasAnyActionLeft && shouldSkipToNextTimelineActions)
	{
		if (m_GameMode != nullptr)
		{
			m_GameMode->UpdatePlayingTeam();
			m_IsEndingTurn = false;
		}
	}
}

//Starts the team turn and assign actions points to each one, or to the whole team if said so
void UTeam::StartTeamTurn()
{
	m_TeamActionsLeft = m_ActionCount;

	/*If the actions aren't made to be played straight (so it means we don't have to keep it in a buffer), then we don't do this*/

	//We have to reset the character's buffer each turn
	if (!m_GameMode->AreActionsPlayingInstantly())
	{
		for (int i = 0; i < m_TeamMembersActionsBuffer.Num(); i++)
		{
			m_TeamMembersActionsBuffer[i].m_Actions = TArray<UTimelineAction*>();
		}
	}

	for (int32 i = 0; i < m_Teammate.Num(); i++)
	{
		m_Teammate[i]->RefreshPossibleActions();
	}
}

//This adds a unit to new arrays
//those arrays are made to keep actions in buffer and to set how many actions the character can do
void UTeam::AddUnit(AProtoCharacter*  _Unit)
{
	//Adding in the buffer of character + actions to do
	FCharacterActions toAdd;
	toAdd.m_ProtoCharacter = _Unit;

	m_TeamMembersActionsBuffer.Add(toAdd);
}

//Remove the unit from every buffers
void UTeam::RemoveUnit(AProtoCharacter*  _Unit)
{
	int index = GetUnitIndexInActionsBuffer(_Unit);
	if (index >= 0)
	{
		m_TeamMembersActionsBuffer.RemoveAt(index);
	}
}

//Checks if the unit can play (checks if it's team or character based as well)
bool UTeam::CanUnitPlay(AProtoCharacter*  _Unit)
{
	return m_TeamActionsLeft > 0;
}

//Registers the action passed as parameter in the buffer
//If the game mode is playing actions instantly, then it's just doing the action instantly
void UTeam::RegisterUnitAction(AProtoCharacter*  _Unit, UTimelineAction *_Action)
{
	if (!m_GameMode->AreActionsPlayingInstantly())
	{
		int index = GetUnitIndexInActionsBuffer(_Unit);
		if (index >= 0)
		{
			UTimelineAction *actionCopy = NewObject<UTimelineAction>(this, _Action->GetClass());
			actionCopy->SetTargetTile(_Unit, _Action->GetTargetTile());
			m_TeamMembersActionsBuffer[index].m_Actions.Add(actionCopy);
		}
	}
	else
	{
		_Unit->DoAction(_Action);
	}
	m_TeamActionsLeft--;
}

void UTeam::UnregisterUnitLastAction(AProtoCharacter *_Unit)
{
	if (!m_GameMode->AreActionsPlayingInstantly())
	{
		int32 characterIndex = GetUnitIndexInActionsBuffer(_Unit);
		if (characterIndex >= 0 && m_TeamMembersActionsBuffer[characterIndex].m_Actions.Num() > 0)
		{
			int32 lastActionIndex = m_TeamMembersActionsBuffer[characterIndex].m_Actions.Num() - 1;

			m_TeamMembersActionsBuffer[characterIndex].m_Actions.RemoveAt(lastActionIndex);
			m_TeamActionsLeft++;
		}
		else
		{
			GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Orange, "You don't have any action to remove.");
		}
	}
}

//Turning on the turn ending flag, also skip to the next team if the game mode are making the actions play instantly
void UTeam::EndTurn()
{
	if (!m_GameMode->AreActionsPlayingInstantly())
	{
		m_IsEndingTurn = true;
		for (int32 i = 0; i < m_TeamMembersActionsBuffer.Num(); i++)
		{
			AProtoCharacter *currentCharacter = m_TeamMembersActionsBuffer[i].m_ProtoCharacter;
			if (m_TeamMembersActionsBuffer[i].m_Actions.Num() > 0)
			{
				currentCharacter->DoAction(m_TeamMembersActionsBuffer[i].m_Actions[0]);
			}
		}
	}
	else
	{
		//Then we tell the game mode we actually ended the turn and ask him to update the playing team
		if (m_GameMode != nullptr)
		{
			m_GameMode->UpdatePlayingTeam();
		}
	}
}

int UTeam::GetUnitIndexInActionsBuffer(AProtoCharacter*  _Unit)
{
	int index = -1;
	for (int i = 0; i < m_TeamMembersActionsBuffer.Num() && index < 0; i++)
	{
		if (m_TeamMembersActionsBuffer[i].m_ProtoCharacter == _Unit)
		{
			index = i;
		}
	}
	return index;
}