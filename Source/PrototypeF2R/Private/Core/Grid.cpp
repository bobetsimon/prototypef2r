// Fill out your copyright notice in the Description page of Project Settings.

#include "Public/Core/Grid.h"
#include "Public/Core/TileComponent.h"
#include "Runtime/Engine/Classes/Materials/MaterialInstance.h"
#include "Runtime/Engine/Classes/Engine/StaticMesh.h"

// Sets default values
AGrid::AGrid()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	USceneComponent *scene = CreateDefaultSubobject<USceneComponent>("Scene");
	if (scene != nullptr)
	{
		RootComponent = scene;
	}
}

void AGrid::OnConstruction(const FTransform &Transform)
{
	Super::OnConstruction(Transform);

	UTileComponent *tile = nullptr;

	/* DONT TOUCH */

	/* Add & remove X */
	for (int32 y = 0; y < m_Lines.Num(); y++)
	{	
		/*Removing in X*/
		while (m_Lines[y].m_Tiles.Num() > m_GridX)
		{
			for (int32 x = 0; x < m_GridX; x++)
			{
				tile = m_Lines[y].m_Tiles[m_Lines[y].m_Tiles.Num() - 1];
				if (tile != nullptr)
				{
					tile->UnregisterComponent();
					tile->DestroyComponent();
				}
			}
			m_Lines[y].m_Tiles.RemoveAt(m_Lines[y].m_Tiles.Num() - 1);
		}

		while (m_Lines[y].m_Tiles.Num() < m_GridX)
		{
			FName name = FName(*FString("Tile" + FString::FromInt(y) + "-" + FString::FromInt(m_Lines[y].m_Tiles.Num())));

			tile = NewObject<UTileComponent>(this, name);
			tile->RegisterComponent();
			if (tile != nullptr)
			{
				tile->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepWorldTransform);
				m_Lines[y].m_Tiles.Add(tile);
				tile->SetCoordinates(m_Lines[y].m_Tiles.Num(), y);
				tile->bEditableWhenInherited = true;
			}
		}
	}

	/* Add & remove Y */

	/*Removing in Y*/
	while (m_Lines.Num() > m_GridY)
	{
		for (int32 x = 0; x < m_GridX; x++)
		{
			tile = m_Lines[m_Lines.Num() - 1].m_Tiles[x];
			if (tile != nullptr)
			{
				tile->UnregisterComponent();
				tile->DestroyComponent();
			}
		}
		m_Lines.RemoveAt(m_Lines.Num() - 1);
	}

	/*Adding in Y*/
	while (m_Lines.Num() < m_GridY)
	{
		FTilesLine line;
		for (int32 x = 0; x < m_GridX; x++)
		{
			FName name = FName(*FString("Tile" + FString::FromInt(m_Lines.Num()) + "-" + FString::FromInt(x)));

			tile = NewObject<UTileComponent>(this, name);
			tile->RegisterComponent();
			if (tile != nullptr)
			{
				tile->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepWorldTransform);
				line.m_Tiles.Add(tile);
				tile->SetCoordinates(x, m_Lines.Num());
				tile->bEditableWhenInherited = true;
			}
		}
		m_Lines.Add(line);
	}

	/* Update created tile parameters */
	for (int32 y = 0; y < m_GridY; y++)
	{
		FTilesLine line;
		UTileComponent *tile = nullptr;

		for (int32 x = 0; x < m_GridX; x++)
		{
			FName name = FName(*FString("Tile" + FString::FromInt(y) + "-" + FString::FromInt(x)));
			tile = GetTile(FIntVector2D(x, y));
			if (tile != nullptr)
			{
				if (tile->m_IsRaised)
				{
					tile->SetRelativeLocation(FVector(x * 100.0f, y * 100.0f, 50.0f));
				}
				else
				{
					tile->SetRelativeLocation(FVector(x * 100.0f, y * 100.0f, 0.0f));
				}
				if (!tile->m_HasBeenSet)
				{
					tile->SetMaterial(0, m_TileBaseColor);
					tile->SetStaticMesh(m_StaticMesh);
					tile->m_BaseColor = m_TileBaseColor;
					tile->m_OtherColor = m_TileOtherColor;
					tile->m_SelectedColor = m_TileSelectedColor;
					tile->m_HasBeenSet = true;
				}
			}
		}
	}
}



void AGrid::DebugOutOfRange()
{
	GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, "Your coodinates are out of grid.");
}

UTileComponent * AGrid::GetTile(FIntVector2D _Coordinates)
{
	if (_Coordinates.X < 0 || _Coordinates.X >= m_GridX ||
		_Coordinates.Y < 0 || _Coordinates.Y >= m_GridY)
	{
		DebugOutOfRange();
		return (nullptr);
	}
	return (m_Lines[_Coordinates.Y].m_Tiles[_Coordinates.X]);
}

UTileComponent * AGrid::GetLeftTile(UTileComponent * _Tile)
{
	if (_Tile != nullptr)
	{
		FIntVector2D offset(-1, 0);

		return (GetTile(_Tile->GetTilePosition() + offset));
	}
	DebugOutOfRange();
	return (nullptr);
}

UTileComponent * AGrid::GetRightTile(UTileComponent * _Tile)
{
	if (_Tile != nullptr)
	{
		return (GetTile(_Tile->GetTilePosition() + FIntVector2D(1, 0)));
	}
	DebugOutOfRange();
	return (nullptr);
}


UTileComponent * AGrid::GetUpTile(UTileComponent * _Tile)
{
	if (_Tile != nullptr)
	{
		return (GetTile(_Tile->GetTilePosition() + FIntVector2D(0, 1)));
	}
	DebugOutOfRange();
	return (nullptr);
}

UTileComponent * AGrid::GetDownTile(UTileComponent * _Tile)
{
	if (_Tile != nullptr)
	{
		return (GetTile(_Tile->GetTilePosition() + FIntVector2D(0, -1)));
	}
	DebugOutOfRange();
	return (nullptr);
}


UTileComponent * AGrid::GetDownRightTile(UTileComponent * _Tile)
{
	if (_Tile != nullptr)
	{
		return (GetTile(_Tile->GetTilePosition() + FIntVector2D(1, -1)));
	}
	DebugOutOfRange();
	return (nullptr);
}

UTileComponent * AGrid::GetUpRightTile(UTileComponent * _Tile)
{
	if (_Tile != nullptr)
	{
		return (GetTile(_Tile->GetTilePosition() + FIntVector2D(1, 1)));
	}
	DebugOutOfRange();
	return (nullptr);
}

UTileComponent * AGrid::GetDownLeftTile(UTileComponent * _Tile)
{
	if (_Tile != nullptr)
	{
		return (GetTile(_Tile->GetTilePosition() + FIntVector2D(-1, -1)));
	}
	DebugOutOfRange();
	return (nullptr);
}

UTileComponent * AGrid::GetUpLeftTile(UTileComponent * _Tile)
{
	if (_Tile != nullptr)
	{
		return (GetTile(_Tile->GetTilePosition() + FIntVector2D(-1, 1)));
	}
	DebugOutOfRange();
	return (nullptr);
}

// Called when the game starts or when spawned
void AGrid::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void AGrid::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

