// Fill out your copyright notice in the Description page of Project Settings.

#include "ProtoCharacter.h"
#include "TileComponent.h"
#include "Ball.h"
#include "Public/TacticalGameMode.h"
#include "Engine/World.h"
#include "EngineUtils.h"
#include "PassAction.h"
#include "MovementAction.h"
#include "Engine/Engine.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BehaviorTree.h"
#include "Runtime/AIModule/Classes/AIController.h"
#include "Runtime/AIModule/Classes/BrainComponent.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BlackboardComponent.h"

// Sets default values
AProtoCharacter::AProtoCharacter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

//This construction script is working so whenever we drag and drop the character, it automatically detect on which tile it is
void AProtoCharacter::OnConstruction(const FTransform &Transform)
{
	Super::OnConstruction(Transform);

	FHitResult hit;
	FCollisionQueryParams params;

	params.AddIgnoredActor(this);

	if (GetWorld()->LineTraceSingleByChannel(hit, Transform.GetLocation(), Transform.GetLocation() + -FVector::UpVector * 651651651, ECollisionChannel::ECC_WorldDynamic, params))
	{
		UTileComponent *hitTile = Cast<UTileComponent>(hit.GetComponent());
		if (hitTile != nullptr)
		{
			m_SpawnCoordinates = hitTile->GetTilePosition();
		}
	}
}

// Called when the game starts or when spawned
void AProtoCharacter::BeginPlay()
{
	Super::BeginPlay();
	//Getting the grid the dirty way
	for (TActorIterator<AGrid> ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		if (*ActorItr != nullptr)
		{
			m_Grid = *ActorItr;
			if (m_Grid != nullptr)
			{
				m_CurrentTile = m_Grid->GetTile(m_SpawnCoordinates);
			}
		}
	}
	//Snapping to the current tile so it looks clean on the grid when the players are spawning
	SnapToCurrentTile();
	// Start on the floor
	m_IsJumping = false;

	StartAI();
}

//Stores the AI controller so we don't have to cast all the time and runs behavior tree
void AProtoCharacter::StartAI()
{
	m_AIController = Cast<AAIController>(GetController());
	if (m_AIController != nullptr && m_BehaviorTreeUsed != nullptr)
	{
		m_AIController->RunBehaviorTree(m_BehaviorTreeUsed);
	}
}

// Called every frame
void AProtoCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AProtoCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AProtoCharacter::SnapBall(ABall *_Ball)
{
	if (_Ball != nullptr)
	{
		_Ball->AttachToActor(this, FAttachmentTransformRules::KeepWorldTransform);
		_Ball->SetPhysics(false);
		m_Ball = _Ball;
	}
	else
	{
		GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Orange, "You're trying to snap a non-existent ball.");
	}
}

void AProtoCharacter::DropBall()
{
	if (m_Ball != nullptr)
	{
		m_Ball->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
		m_Ball = nullptr;
	}
	else
	{
		GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Orange, "You must have the ball drop it...");
	}
}

void AProtoCharacter::Shoot()
{
	if (m_Ball != nullptr)
	{
		m_Ball->GetShot(m_CurrentTile, m_TargetTile);
		DropBall();
	}
	else
	{
		GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Orange, "You must have the ball to shoot...");
	}
}

void AProtoCharacter::DoAction(UTimelineAction *_Action)
{
	if (_Action != nullptr)
	{
		_Action->Execute(this);
	}
}

void AProtoCharacter::SnapToCurrentTile()
{
	if (m_CurrentTile != nullptr)
	{
		FVector locationToSnap = m_CurrentTile->GetComponentLocation();
		locationToSnap.Z = GetActorLocation().Z;
		SetActorLocation(locationToSnap, false);
		ABall *ball = Cast<ABall>(m_CurrentTile->GetHandledEntity());

		//This ain't needed but it's better than seeing a warning debug message that tells you 
		//"you gotta have the ball to snap it"
		if (ball != nullptr)
		{
			SnapBall(ball);
		}
		m_CurrentTile->SetHandledEntity(this);
	}
}

//Refreshes every possible actions (example : if the character doesn't have the ball, he won't have as much actions possible)
void AProtoCharacter::RefreshPossibleActions()
{
	m_PossibleActions.Empty();

	m_PossibleActions.Add(NewObject<UPassAction>(this, UPassAction::StaticClass()));
	m_PossibleActions.Add(NewObject<UMovementAction>(this, UMovementAction::StaticClass()));
}

//Simply sets a variable in the behavior tree, then it'll just manage itself...
void AProtoCharacter::ChangeCurrentAction(EActionEnum _Action)
{
	if (m_AIController != nullptr)
	{
		const UEnum* EnumPtr = FindObject<UEnum>(ANY_PACKAGE, TEXT("EActionEnum"), true);
		GEngine->AddOnScreenDebugMessage(-1, 2.0, FColor::White, EnumPtr->GetNameByValue((int64)_Action).ToString());
		m_AIController->GetBrainComponent()->GetBlackboardComponent()->SetValueAsEnum("ActionEnum", (uint8)_Action);
	}
}

void AProtoCharacter::ChangeMoveTarget(const FVector &_Target)
{
	if (m_AIController != nullptr)
	{
		m_AIController->GetBrainComponent()->GetBlackboardComponent()->SetValueAsVector("MoveLocation", _Target);
	}
}

EActionEnum AProtoCharacter::GetCurrentActionState()
{
	if (m_AIController != nullptr)
	{
		return ((EActionEnum)m_AIController->GetBrainComponent()->GetBlackboardComponent()->GetValueAsEnum("ActionEnum"));
	}
	return (EActionEnum::AE_None);
}

UTileComponent	*AProtoCharacter::GetTileUnderCharacter()
{
	FHitResult hit;
	FCollisionQueryParams params;

	params.AddIgnoredActor(this);

	if (GetWorld()->LineTraceSingleByChannel(hit, GetTransform().GetLocation(), GetTransform().GetLocation() + -FVector::UpVector * 651651651, ECollisionChannel::ECC_WorldDynamic, params))
	{
		UTileComponent *hitTile = Cast<UTileComponent>(hit.GetComponent());
		if (hitTile != nullptr)
		{
			return (hitTile);
		}
	}
	return (nullptr);
}