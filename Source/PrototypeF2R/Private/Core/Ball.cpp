// Fill out your copyright notice in the Description page of Project Settings.

#include "Ball.h"
#include "TileComponent.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include "Components/StaticMeshComponent.h"
#include "ProtoCharacter.h"
#include "Runtime/Engine/Classes/Kismet/KismetMathLibrary.h"

// Sets default values
ABall::ABall()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	m_BallMesh = CreateDefaultSubobject<UStaticMeshComponent>("Ball");
	if (m_BallMesh != nullptr)
	{
		RootComponent = m_BallMesh;
	}
}

// Called when the game starts or when spawned
void ABall::BeginPlay()
{
	Super::BeginPlay();

	m_BallMesh->OnComponentBeginOverlap.AddDynamic(this, &ABall::OnOverlap);
}

// Called every frame
void ABall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABall::GetShot(const UTileComponent *_Start, const UTileComponent *_End)
{
	if (_Start != nullptr && _End != nullptr)
	{
		GetShot(_Start->GetComponentLocation(), _End->GetComponentLocation());
	}
}

void ABall::GetShot(const FVector &_Start, const FVector &_End)
{
	m_BallMesh->SetSimulatePhysics(true);

	FVector directionToTarget = _End - _Start;
	float Pz = directionToTarget.Z;
	directionToTarget.Z = 0;
	float Px = directionToTarget.Size();

	float numerator = GetWorld()->GetGravityZ() * Px * Px;
	float denumerator = 2 * FMath::Cos(FMath::DegreesToRadians(45.0f)) * FMath::Cos(FMath::DegreesToRadians(45.0f)) * (Pz - FMath::Tan(FMath::DegreesToRadians(45.0f)) * Px);
	float power = FMath::Sqrt(numerator / denumerator);

	FVector crossXY = FVector::CrossProduct(directionToTarget, FVector::UpVector);
	FRotator rotation = UKismetMathLibrary::RotatorFromAxisAndAngle(crossXY, 45.0f);
	directionToTarget = rotation.RotateVector(directionToTarget);
	FVector force = directionToTarget.GetSafeNormal() * power;

	if (m_BallMesh != nullptr)
	{
		m_BallMesh->SetAllPhysicsLinearVelocity(force);
	}
}

void ABall::OnOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	UTileComponent *tile = Cast<UTileComponent>(OtherComp);

	if (tile != nullptr)
	{
		AProtoCharacter *character = Cast<AProtoCharacter>(tile->GetHandledEntity());
		if (character != nullptr)
		{
			character->SnapBall(this);
		}
		else
		{
			float zLocation = GetActorLocation().Z;
			tile->SetHandledEntity(this);
			m_BallMesh->SetSimulatePhysics(false);
			SetActorLocation(FVector(tile->GetComponentLocation().X, tile->GetComponentLocation().Y, zLocation));
		}
	}
}

void ABall::SetPhysics(bool _IsActive)
{
	m_BallMesh->SetSimulatePhysics(_IsActive);
}