// Fill out your copyright notice in the Description page of Project Settings.

#include "TileComponent.h"


void UTileComponent::SetSelected(bool _IsSelected)
{
	if (GetStaticMesh() != nullptr)
	{
		if (_IsSelected)
		{
			SetMaterial(0, m_SelectedColor);
		}
		else
		{
			SetMaterial(0, m_BaseColor);
		}
	}
}

void UTileComponent::SetCoordinates(int32 _X, int32 _Y)
{
	m_Coordinates.X = _X;
	m_Coordinates.Y = _Y;
}
