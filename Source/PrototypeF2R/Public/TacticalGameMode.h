// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "TacticalGameMode.generated.h"

class UTeam;
class ACursor;

UCLASS()
class PROTOTYPEF2R_API ATacticalGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	virtual void BeginPlay() override;

	virtual void Tick(float _DeltaTime) override;

	FORCEINLINE bool			AreActionsLimitedForEachCharacter() { return (m_AreActionsLimitedForEachCharacter); }
	FORCEINLINE bool			AreActionsPlayingInstantly() { return (m_PlayActionsInstantanly); }
	FORCEINLINE void			SetShouldReorderTeam(bool _ShouldReorder) { m_ShouldReorder = _ShouldReorder; }
	FORCEINLINE bool			ShouldReorderTeam() { return (m_ShouldReorder); }

	FORCEINLINE TArray<UTeam*>	&GetTeams() { return (m_Teams); }

	void UpdatePlayingTeam();
	void ReorderTeams();

protected:
	UPROPERTY()
		TArray<UTeam*> m_Teams;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		ACursor *m_Cursor = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		bool m_AreActionsLimitedForEachCharacter = false;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		bool m_PlayActionsInstantanly = false;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		bool m_ShouldReorder = false;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		UTeam *m_CurrentPlayingTeam = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		int32 m_CurrentTeamIndex = 0;
};
