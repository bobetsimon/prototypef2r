// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Core/Actions/TimelineAction.h"
#include "WaitAction.generated.h"

/**
 * 
 */
UCLASS()
class PROTOTYPEF2R_API UWaitAction : public UTimelineAction
{
	GENERATED_BODY()
	
public:
	UWaitAction();
	
};
