// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "TimelineAction.generated.h"

class AProtoCharacter;
class UTileComponent;

UENUM(BlueprintType)
enum class EActionEnum : uint8
{
	AE_None UMETA(DisplayName = "None"),
	AE_Movement UMETA(DisplayName = "Movement"),
	AE_Pass UMETA(DisplayName = "Pass")
};

UCLASS(Blueprintable)
class PROTOTYPEF2R_API UTimelineAction : public UObject
{
	GENERATED_BODY()

public:
	UTileComponent *GetTargetTile() { return (m_TargetTile); }

	void SetTargetTile(AProtoCharacter *_CharacterToExecuteAction, UTileComponent *_Tile);

	virtual void Execute(AProtoCharacter *_CharacterToExecuteAction);

protected:
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
		UTileComponent *m_TargetTile = nullptr;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
		FString m_ActionName = "";

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
		EActionEnum m_ActionEnumValue = EActionEnum::AE_None;
};
