// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Team.generated.h"

class AProtoCharacter;
class UCharacterAction;
class ATacticalGameMode;
class UTimelineAction;

USTRUCT(BlueprintType)
struct FCharacterActions
{
	GENERATED_BODY()

public:
	UPROPERTY()
		AProtoCharacter *m_ProtoCharacter = nullptr;

	UPROPERTY()
		TArray<UTimelineAction*> m_Actions;
};

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class PROTOTYPEF2R_API UTeam : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UTeam();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	//Starts the team turn and assign actions points to each one, or to the whole team if said so
	void StartTeamTurn();

	//we'll keep playing all the actions of the time line while there are some to play
	void ManageTurnEnding();

	//This adds a unit to new arrays
	//those arrays are made to keep actions in buffer and to set how many actions the character can do
	void AddUnit(AProtoCharacter* _Unit);

	//Remove the unit from every buffers
	void RemoveUnit(AProtoCharacter* _Unit);

	//Checks if the unit can play (checks if it's team or character based as well)
	bool CanUnitPlay(AProtoCharacter* _Unit);

	//Registers the action passed as parameter in the buffer
	//If the game mode is playing actions instantly, then it's just doing the action instantly
	void RegisterUnitAction(AProtoCharacter* _Unit, UTimelineAction *_Action);

	void UnregisterUnitLastAction(AProtoCharacter *_Unit);

	void EndTurn();

	FORCEINLINE int32 GetTeamID() const { return (m_TeamID); }
	FORCEINLINE TArray<AProtoCharacter*> &GetTeammates() { return (m_Teammate); }
	FORCEINLINE TArray<FCharacterActions> &GetTeammatesActionsBuffer() { return (m_TeamMembersActionsBuffer); }
	FORCEINLINE bool IsEndingTurn() { return (m_IsEndingTurn); }

	int GetUnitIndexInActionsBuffer(AProtoCharacter* _Unit);

protected:
	UPROPERTY()
		TArray<FCharacterActions> m_TeamMembersActionsBuffer;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Proto Team")
		int32 m_TeamID = 0;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Proto Team")
		int32 m_ActionCount = 3;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Proto Team")
		TArray<AProtoCharacter*> m_Teammate;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
		int32 m_TeamActionsLeft;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
		bool			m_IsEndingTurn = false;

private:
	UPROPERTY()
		ATacticalGameMode *m_GameMode = nullptr;
};
