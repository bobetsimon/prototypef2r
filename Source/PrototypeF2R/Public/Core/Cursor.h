// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Cursor.generated.h"

class UTileComponent;
class UTeam;
class ATacticalGameMode;
class AProtoCharacter;
class UTimelineAction;

UCLASS()
class PROTOTYPEF2R_API ACursor : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ACursor();

	void ResetAction();


	//Blueprint implementable event to select the character we're using right now
	//used for current character's feedback
	UFUNCTION(BlueprintImplementableEvent)
		void ShowCharacterSelectionFeedback(bool _ResetTimeline = false);

	//same but for actions
	UFUNCTION(BlueprintImplementableEvent)
		void ShowActionSelectionFeedback(bool _ResetTimeline = false);

	UFUNCTION(BlueprintImplementableEvent)
		void RemoveLastActionFromTimeline(AProtoCharacter *_Character);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;


	//Inputs
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	void MoveRight(float _Value);
	void MoveUp(float _Value);

	//Refreshes current, changing the new and old ones
	void RefreshCurrentTile();

	//Checks if our cursor is currently moving by the player input
	bool IsOnMovement();

	//Snaps the cursor on the current tile (for better controls of what we're doing)
	void SnapOnCurrentTile();

	//Switch to next character
	void SwitchToNextCharacter();

	//Switch to previous character
	void SwitchToPreviousCharacter();

	//This is called most likely only once, or at least until the team is set, also prints what's going wrong whenever it needs to
	void FirstTeamInitialization();

	//Cancels the current character's action
	void CancelCurrentCharacterAction();

	void ConfirmAction();

	void CancelAction();

	void EndTurn();

	void SwitchToPreviousAction();

	void SwitchToNextAction();

	bool IsTurnEnding();

	void SwitchAction(bool _IsNext);

	//Adds the action passed as parameter in the time line, the event itself manages the time line board and everything...
	UFUNCTION(BlueprintImplementableEvent)
		void AddActionInTimeline(UTimelineAction *_Action, AProtoCharacter *_Character);


	UPROPERTY(EditAnywhere)
		float m_CursorSpeed = 0;

	UPROPERTY(EditAnywhere, meta = (ClampMin = "0.0"))
		float m_SnappingSpeed = 1.0f;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		UTileComponent *m_CurrentTile = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		UTileComponent *m_OldTile = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		UTeam			*m_CurrentTeam = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		ATacticalGameMode *m_GameMode = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		AProtoCharacter *m_CurrentCharacter = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		int32			m_CurrentCharacterIndex = 0;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		int32			m_CurrentActionIndex = 0;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		UTimelineAction *m_CurrentSelectedAction = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		int32			m_CurrentSelectionActionIndex = 0;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		UTimelineAction *m_CurrentConfirmedAction = nullptr;

private:
	//Used to switch to the next or previous character depending on the boolean passed as parameter
	void SwitchCharacter(bool _IsNext);
};