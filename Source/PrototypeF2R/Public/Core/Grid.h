// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Grid.generated.h"

class UTileComponent;
class UTileComponent;
class UStaticMesh;
class UMaterialInstance;

USTRUCT(BlueprintType, Atomic)
struct FTilesLine
{
	GENERATED_BODY()
public:

	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (AllowPrivateAccess = "true"))
		TArray<UTileComponent*> m_Tiles;
};


USTRUCT(BlueprintType)
struct FIntVector2D
{
	GENERATED_BODY()

public:

	FIntVector2D()
	{
		X = 0;
		Y = 0;
	}

	FIntVector2D(int32 _X, int32 _Y)
	{
		X = _X;
		Y = _Y;
	}
	FIntVector2D operator+(const FIntVector2D& _V2) const
	{
		return (FIntVector2D(X + _V2.X, Y + _V2.Y));
	}

	FIntVector2D operator-(const FIntVector2D& _V2) const
	{
		return (FIntVector2D(X - _V2.X, Y - _V2.Y));
	}

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int32 X;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int32 Y;
};

UCLASS()
class PROTOTYPEF2R_API AGrid : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AGrid();

	//Returns the tile of the grid using coordinates passed as parameter
	UFUNCTION(BlueprintPure)
		UTileComponent *GetTile(FIntVector2D _Coordinates);

	UFUNCTION(BlueprintPure)
		UTileComponent *GetLeftTile(UTileComponent *_Tile);

	UFUNCTION(BlueprintPure)
		UTileComponent *GetDownTile(UTileComponent *_Tile);

	UFUNCTION(BlueprintPure)
		UTileComponent *GetUpTile(UTileComponent *_Tile);

	UFUNCTION(BlueprintPure)
		UTileComponent *GetRightTile(UTileComponent *_Tile);

	UFUNCTION(BlueprintPure)
		UTileComponent *GetDownRightTile(UTileComponent *_Tile);

	UFUNCTION(BlueprintPure)
		UTileComponent *GetUpRightTile(UTileComponent *_Tile);

	UFUNCTION(BlueprintPure)
		UTileComponent *GetDownLeftTile(UTileComponent *_Tile);

	UFUNCTION(BlueprintPure)
		UTileComponent *GetUpLeftTile(UTileComponent *_Tile);

	void DebugOutOfRange();

	virtual void OnConstruction(const FTransform &Transform) override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		UStaticMesh *m_StaticMesh = nullptr;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int32 m_GridX = 10;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int32 m_GridY = 20;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float m_TileScale = 100.0f;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (AllowPrivateAccess = "true"))
		TArray<FTilesLine> m_Lines;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		UMaterialInstance *m_TileBaseColor = nullptr;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		UMaterialInstance *m_TileSelectedColor = nullptr;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		UMaterialInstance *m_TileOtherColor = nullptr;
};
