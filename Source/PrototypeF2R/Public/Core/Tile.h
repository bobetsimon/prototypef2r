// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Grid.h"
#include "Tile.generated.h"

class UStaticMesh;
class UStaticMeshComponent;
class UMaterialInstance;

UCLASS()
class PROTOTYPEF2R_API ATile : public AActor
{

	GENERATED_BODY()


public:
	// Sets default values for this actor's properties
	ATile();

	UFUNCTION(BlueprintPure)
		AActor *GetHandledEntity() { return (m_Actor); }

	UFUNCTION(BlueprintCallable)
		void	SetHandledEntity(AActor *_Actor) { m_Actor = _Actor; }

	UFUNCTION(BlueprintCallable)
		void	SetSelected(bool _IsSelected);

	UFUNCTION(BlueprintPure)
		FIntVector2D GetTilePosition() { return (m_Coordinates); }

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;




	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
		bool m_IsRaised;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
		AActor *m_Actor = nullptr;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
		UStaticMeshComponent *m_StaticMeshComponent = nullptr;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		UMaterialInstance *m_BaseColor = nullptr;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		UMaterialInstance *m_SelectedColor = nullptr;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		UMaterialInstance *m_OtherColor = nullptr;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
		FIntVector2D		m_Coordinates;
};
