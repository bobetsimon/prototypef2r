// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Grid.h"
#include "TimelineAction.h"
#include "ProtoCharacter.generated.h"

class UTileComponent;
class UCharacterAction;
class ABall;
class ATacticalGameMode;
class AGrid;
class UTimelineAction;
class UBehaviorTree;
class AAIController;

UCLASS()
class PROTOTYPEF2R_API AProtoCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AProtoCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// Execute current action
	void DoAction(UTimelineAction *_Action);

	void StartAI();

	UFUNCTION(BlueprintCallable, BlueprintPure)
		bool IsJumping() { return m_IsJumping; }

	UFUNCTION(BlueprintCallable)
		void SetCurrentTile(UTileComponent* _Value) { m_CurrentTile = _Value; }

	UFUNCTION(BlueprintCallable, BlueprintPure)
		UTileComponent* GetCurrentTile() { return m_CurrentTile; }

	UFUNCTION(BlueprintCallable, BlueprintPure)
		int32 GetNbAction() { return m_NbAction; }

	void SnapBall(ABall *_Ball);

	void DropBall();

	void Shoot();

	UFUNCTION(BlueprintCallable)
		void SnapToCurrentTile();

	void RefreshPossibleActions();

	UFUNCTION(BlueprintCallable)
		void ChangeCurrentAction(EActionEnum _Action);

	UFUNCTION(BlueprintCallable)
		void ChangeMoveTarget(const FVector &_Target);

	UFUNCTION(BlueprintPure)
		UTileComponent *GetTileUnderCharacter();

	EActionEnum	GetCurrentActionState();

	FORCEINLINE TArray<UTimelineAction*> GetPossibleActions() { return (m_PossibleActions); }

protected:

	virtual void OnConstruction(const FTransform &Transform) override;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Proto Actions")
		int32 m_NbAction;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "Proto Actions")
		UTileComponent* m_TargetTile = nullptr;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "Proto Actions")
		UTileComponent* m_CurrentTile = nullptr;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "Proto Actions")
		bool m_IsJumping;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
		ABall *m_Ball = nullptr;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
		FIntVector2D m_SpawnCoordinates;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
		AGrid		*m_Grid = nullptr;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
		ATacticalGameMode *m_GameMode = nullptr;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		FString			m_CharacterName = "Tactical_Character";

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
		TArray<UTimelineAction*> m_PossibleActions;

	UPROPERTY(BlueprintReadOnly, EditAnywhere)
		UBehaviorTree		*m_BehaviorTreeUsed = nullptr;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
		AAIController		*m_AIController = nullptr;
};
