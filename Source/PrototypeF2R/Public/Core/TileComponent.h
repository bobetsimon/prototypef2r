// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/StaticMeshComponent.h"
#include "Grid.h"
#include "TileComponent.generated.h"

class UMaterialInstance;

UCLASS(Blueprintable, editinlinenew, meta = (BlueprintSpawnableComponent))
class PROTOTYPEF2R_API UTileComponent : public UStaticMeshComponent
{
	GENERATED_BODY()

		friend class AGrid;

public:

	UFUNCTION(BlueprintPure)
		AActor *GetHandledEntity() { return (m_Actor); }

	UFUNCTION(BlueprintCallable)
		void	SetHandledEntity(AActor *_Actor) { m_Actor = _Actor; }

	UFUNCTION(BlueprintCallable)
		void	SetSelected(bool _IsSelected);

	UFUNCTION(BlueprintPure)
		FIntVector2D GetTilePosition() { return (m_Coordinates); }

protected:

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		bool m_IsRaised;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
		AActor *m_Actor = nullptr;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		UMaterialInstance *m_BaseColor = nullptr;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		UMaterialInstance *m_SelectedColor = nullptr;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		UMaterialInstance *m_OtherColor = nullptr;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
		FIntVector2D		m_Coordinates;

private:
	void SetCoordinates(int32 _X, int32 _Y);

	bool m_HasBeenSet = false;
};
