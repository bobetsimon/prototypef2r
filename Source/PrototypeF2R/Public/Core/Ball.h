// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Ball.generated.h"

class UTileComponent;
class UStaticMeshComponent;

UCLASS()
class PROTOTYPEF2R_API ABall : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ABall();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
		void GetShot(const UTileComponent *_Start, const UTileComponent *_End);

	void GetShot(const FVector &_Start, const FVector &_End);

	void SetPhysics(bool _IsActive);

protected:

	UFUNCTION()
		void OnOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
		UStaticMeshComponent *m_BallMesh = nullptr;
};
